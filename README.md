# Varnishlog Digest

Reads the log for some time, prints a summary of what happened on exit:

    root@hermes:~/varnishlog-digest# ./varnishlog-digest
        Reading from 'varnishlog -c'; hit Ctrl-C to stop reading and print digest
    ^C     57 Requests, 36 seconds
    
    HTTP Host headers:
           37 freakytrigger.co.uk
           20 www.quackometer.net
    
    Remote addresses:
           17 103.53.230.75
            4 216.244.66.248
            3 207.46.13.35
            3 82.18.222.74
            3 165.225.80.79
            3 207.46.13.38
            3 85.126.148.186
            2 207.46.13.36
            2 141.8.142.93
            2 159.65.224.212
    
    IP/site combinations:
           17 103.53.230.75   freakytrigger.co.uk
            4 216.244.66.248  www.quackometer.net
            3 82.18.222.74    www.quackometer.net
            3 207.46.13.38    freakytrigger.co.uk
            3 85.126.148.186  www.quackometer.net
            3 165.225.80.79   www.quackometer.net
            3 207.46.13.35    freakytrigger.co.uk
            2 159.65.224.212  www.quackometer.net
            2 141.8.142.93    freakytrigger.co.uk
            2 207.46.13.36    freakytrigger.co.uk
    root@hermes:~/varnishlog-digest#
